﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesala
{
    public partial class Form1 : Form
    {
        List<String> listaRijeci = new List<String>();
        string path = @"C:\Users\Sandro Blavicki\Desktop\LV6\Vjesala\Lista.txt";
        string tajnaRijec="";
        int brojRijeci = 0;
        int randomBroj=0;
        int zivot = 5;
        bool provjeraSlova = false;
        int brojac = 0;

        

        public Form1()
        {
            InitializeComponent();
        }

        private void letter_click(object sender, EventArgs e)
        {
            brojac = 0;
            provjeraSlova = false;
            Button button = (Button)sender;
            button.Enabled = false;

            StringBuilder novaRijec = new StringBuilder(pogadanaRijec.Text);

            foreach (char c in tajnaRijec)
            {
                if (c == button.Text[0])
                {
                    novaRijec[brojac] = button.Text[0];
                    pogadanaRijec.Text = novaRijec.ToString();
                    provjeraSlova = true;
                }
                brojac++;
            }

            if (!provjeraSlova)
            {
                zivot--;
            }

            brojZivota.Text = zivot.ToString();

            if (tajnaRijec == pogadanaRijec.Text)
                MessageBox.Show("Pogodili ste rijec", "Bravo");

            if (zivot <= 0)
            {
                MessageBox.Show("Izgubili ste", "KRAJ");
            }

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {
                    listaRijeci.Add(line);
                    brojRijeci++;
                }
            }
            randomRijec();
        }

        private void randomRijec()
        {
            Random random = new Random();
            randomBroj = random.Next(brojRijeci);
            tajnaRijec = listaRijeci[randomBroj];

            for (int i = 0; i < tajnaRijec.Length; i++)
                pogadanaRijec.Text += "-";
        }

        private void newGame_Click(object sender, EventArgs e)
        {
            brojac = 0;
            pogadanaRijec.Text = "";
            zivot = 5;
            brojZivota.Text = zivot.ToString();
            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    
                }
                catch { }
            }
            randomRijec();
        }
    }
}
