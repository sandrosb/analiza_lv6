﻿namespace Vjesala
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.brojZivota = new System.Windows.Forms.Label();
            this.pogadanaRijec = new System.Windows.Forms.Label();
            this.newGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(38, 199);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "A";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.letter_click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 199);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "B";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.letter_click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(130, 199);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 40);
            this.button3.TabIndex = 2;
            this.button3.Text = "C";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.letter_click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(176, 199);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(40, 40);
            this.button4.TabIndex = 3;
            this.button4.Text = "D";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.letter_click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(222, 199);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(40, 40);
            this.button5.TabIndex = 4;
            this.button5.Text = "E";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.letter_click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(268, 199);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 40);
            this.button6.TabIndex = 5;
            this.button6.Text = "F";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.letter_click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(314, 199);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 40);
            this.button7.TabIndex = 6;
            this.button7.Text = "G";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.letter_click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(360, 199);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(40, 40);
            this.button8.TabIndex = 7;
            this.button8.Text = "H";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.letter_click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(360, 245);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(40, 40);
            this.button9.TabIndex = 15;
            this.button9.Text = "P";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.letter_click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(314, 245);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(40, 40);
            this.button10.TabIndex = 14;
            this.button10.Text = "O";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.letter_click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(268, 245);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(40, 40);
            this.button11.TabIndex = 13;
            this.button11.Text = "N";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.letter_click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(222, 245);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(40, 40);
            this.button12.TabIndex = 12;
            this.button12.Text = "M";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.letter_click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(176, 245);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(40, 40);
            this.button13.TabIndex = 11;
            this.button13.Text = "L";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.letter_click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(130, 245);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(40, 40);
            this.button14.TabIndex = 10;
            this.button14.Text = "K";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.letter_click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(84, 245);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(40, 40);
            this.button15.TabIndex = 9;
            this.button15.Text = "J";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.letter_click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(38, 245);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(40, 40);
            this.button16.TabIndex = 8;
            this.button16.Text = "I";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.letter_click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(360, 291);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(40, 40);
            this.button17.TabIndex = 23;
            this.button17.Text = "X";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.letter_click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(314, 291);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(40, 40);
            this.button18.TabIndex = 22;
            this.button18.Text = "W";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.letter_click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(268, 291);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(40, 40);
            this.button19.TabIndex = 21;
            this.button19.Text = "V";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.letter_click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(222, 291);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(40, 40);
            this.button20.TabIndex = 20;
            this.button20.Text = "U";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.letter_click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(176, 291);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(40, 40);
            this.button21.TabIndex = 19;
            this.button21.Text = "T";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.letter_click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(130, 291);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(40, 40);
            this.button22.TabIndex = 18;
            this.button22.Text = "S";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.letter_click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(84, 291);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(40, 40);
            this.button23.TabIndex = 17;
            this.button23.Text = "R";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.letter_click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(38, 291);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(40, 40);
            this.button24.TabIndex = 16;
            this.button24.Text = "Q";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.letter_click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(84, 337);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(40, 40);
            this.button25.TabIndex = 25;
            this.button25.Text = "Z";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.letter_click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(38, 337);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(40, 40);
            this.button26.TabIndex = 24;
            this.button26.Text = "Y";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.letter_click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(418, 213);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Broj zivota:";
            // 
            // brojZivota
            // 
            this.brojZivota.AutoSize = true;
            this.brojZivota.Location = new System.Drawing.Point(483, 213);
            this.brojZivota.Name = "brojZivota";
            this.brojZivota.Size = new System.Drawing.Size(13, 13);
            this.brojZivota.TabIndex = 27;
            this.brojZivota.Text = "5";
            // 
            // pogadanaRijec
            // 
            this.pogadanaRijec.AutoSize = true;
            this.pogadanaRijec.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pogadanaRijec.Location = new System.Drawing.Point(25, 82);
            this.pogadanaRijec.Name = "pogadanaRijec";
            this.pogadanaRijec.Size = new System.Drawing.Size(0, 73);
            this.pogadanaRijec.TabIndex = 28;
            // 
            // newGame
            // 
            this.newGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newGame.Location = new System.Drawing.Point(314, 339);
            this.newGame.Name = "newGame";
            this.newGame.Size = new System.Drawing.Size(235, 44);
            this.newGame.TabIndex = 29;
            this.newGame.Text = "Nova Igra";
            this.newGame.UseVisualStyleBackColor = true;
            this.newGame.Click += new System.EventHandler(this.newGame_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 395);
            this.Controls.Add(this.newGame);
            this.Controls.Add(this.pogadanaRijec);
            this.Controls.Add(this.brojZivota);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label brojZivota;
        private System.Windows.Forms.Label pogadanaRijec;
        private System.Windows.Forms.Button newGame;
    }
}

