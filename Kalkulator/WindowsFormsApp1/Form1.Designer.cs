﻿namespace WindowsFormsApp1
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.b_sin = new System.Windows.Forms.Button();
            this.b_cos = new System.Windows.Forms.Button();
            this.b_pow = new System.Windows.Forms.Button();
            this.b_sqrt = new System.Windows.Forms.Button();
            this.b_plus = new System.Windows.Forms.Button();
            this.b_minus = new System.Windows.Forms.Button();
            this.b_mult = new System.Windows.Forms.Button();
            this.b_div = new System.Windows.Forms.Button();
            this.b_log = new System.Windows.Forms.Button();
            this.b_enter = new System.Windows.Forms.Button();
            this.rezultat = new System.Windows.Forms.TextBox();
            this.trenutnaFunkcija = new System.Windows.Forms.Label();
            this.clear_erase = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(43, 168);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 50);
            this.button1.TabIndex = 0;
            this.button1.Text = "7";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(99, 168);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 50);
            this.button2.TabIndex = 1;
            this.button2.Text = "8";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(155, 168);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(50, 50);
            this.button3.TabIndex = 2;
            this.button3.Text = "9";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(155, 224);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 50);
            this.button4.TabIndex = 5;
            this.button4.Text = "6";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(99, 224);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 50);
            this.button5.TabIndex = 4;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button_click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(43, 224);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 50);
            this.button6.TabIndex = 3;
            this.button6.Text = "4";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button_click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(155, 280);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(50, 50);
            this.button7.TabIndex = 8;
            this.button7.Text = "3";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button_click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(99, 280);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 50);
            this.button8.TabIndex = 7;
            this.button8.Text = "2";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button_click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(43, 280);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 50);
            this.button9.TabIndex = 6;
            this.button9.Text = "1";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button_click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(43, 336);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(106, 50);
            this.button10.TabIndex = 9;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button_click);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(155, 336);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(50, 50);
            this.button11.TabIndex = 10;
            this.button11.Text = ".";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button_click);
            // 
            // b_sin
            // 
            this.b_sin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_sin.Location = new System.Drawing.Point(43, 112);
            this.b_sin.Name = "b_sin";
            this.b_sin.Size = new System.Drawing.Size(50, 50);
            this.b_sin.TabIndex = 11;
            this.b_sin.Text = "sin ";
            this.b_sin.UseVisualStyleBackColor = true;
            this.b_sin.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_cos
            // 
            this.b_cos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_cos.Location = new System.Drawing.Point(99, 112);
            this.b_cos.Name = "b_cos";
            this.b_cos.Size = new System.Drawing.Size(50, 50);
            this.b_cos.TabIndex = 12;
            this.b_cos.Text = "cos";
            this.b_cos.UseVisualStyleBackColor = true;
            this.b_cos.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_pow
            // 
            this.b_pow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_pow.Location = new System.Drawing.Point(155, 112);
            this.b_pow.Name = "b_pow";
            this.b_pow.Size = new System.Drawing.Size(50, 50);
            this.b_pow.TabIndex = 13;
            this.b_pow.Text = "pow";
            this.b_pow.UseVisualStyleBackColor = true;
            this.b_pow.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_sqrt
            // 
            this.b_sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_sqrt.Location = new System.Drawing.Point(211, 112);
            this.b_sqrt.Name = "b_sqrt";
            this.b_sqrt.Size = new System.Drawing.Size(50, 50);
            this.b_sqrt.TabIndex = 14;
            this.b_sqrt.Text = "sqrt";
            this.b_sqrt.UseVisualStyleBackColor = true;
            this.b_sqrt.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_plus
            // 
            this.b_plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_plus.Location = new System.Drawing.Point(211, 168);
            this.b_plus.Name = "b_plus";
            this.b_plus.Size = new System.Drawing.Size(50, 50);
            this.b_plus.TabIndex = 15;
            this.b_plus.Text = "+";
            this.b_plus.UseVisualStyleBackColor = true;
            this.b_plus.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_minus
            // 
            this.b_minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_minus.Location = new System.Drawing.Point(211, 224);
            this.b_minus.Name = "b_minus";
            this.b_minus.Size = new System.Drawing.Size(50, 50);
            this.b_minus.TabIndex = 16;
            this.b_minus.Text = "-";
            this.b_minus.UseVisualStyleBackColor = true;
            this.b_minus.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_mult
            // 
            this.b_mult.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_mult.Location = new System.Drawing.Point(211, 280);
            this.b_mult.Name = "b_mult";
            this.b_mult.Size = new System.Drawing.Size(50, 50);
            this.b_mult.TabIndex = 17;
            this.b_mult.Text = "*";
            this.b_mult.UseVisualStyleBackColor = true;
            this.b_mult.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_div
            // 
            this.b_div.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_div.Location = new System.Drawing.Point(211, 336);
            this.b_div.Name = "b_div";
            this.b_div.Size = new System.Drawing.Size(50, 50);
            this.b_div.TabIndex = 18;
            this.b_div.Text = "/";
            this.b_div.UseVisualStyleBackColor = true;
            this.b_div.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_log
            // 
            this.b_log.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_log.Location = new System.Drawing.Point(267, 112);
            this.b_log.Name = "b_log";
            this.b_log.Size = new System.Drawing.Size(50, 50);
            this.b_log.TabIndex = 19;
            this.b_log.Text = "log";
            this.b_log.UseVisualStyleBackColor = true;
            this.b_log.Click += new System.EventHandler(this.klik_operator);
            // 
            // b_enter
            // 
            this.b_enter.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_enter.Location = new System.Drawing.Point(267, 280);
            this.b_enter.Name = "b_enter";
            this.b_enter.Size = new System.Drawing.Size(50, 106);
            this.b_enter.TabIndex = 20;
            this.b_enter.Text = "=";
            this.b_enter.UseVisualStyleBackColor = true;
            this.b_enter.Click += new System.EventHandler(this.b_enter_Click);
            // 
            // rezultat
            // 
            this.rezultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rezultat.Location = new System.Drawing.Point(43, 66);
            this.rezultat.Name = "rezultat";
            this.rezultat.Size = new System.Drawing.Size(274, 29);
            this.rezultat.TabIndex = 21;
            this.rezultat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // trenutnaFunkcija
            // 
            this.trenutnaFunkcija.AutoSize = true;
            this.trenutnaFunkcija.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trenutnaFunkcija.Location = new System.Drawing.Point(251, 39);
            this.trenutnaFunkcija.Name = "trenutnaFunkcija";
            this.trenutnaFunkcija.Size = new System.Drawing.Size(0, 24);
            this.trenutnaFunkcija.TabIndex = 22;
            // 
            // clear_erase
            // 
            this.clear_erase.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear_erase.Location = new System.Drawing.Point(267, 168);
            this.clear_erase.Name = "clear_erase";
            this.clear_erase.Size = new System.Drawing.Size(50, 50);
            this.clear_erase.TabIndex = 23;
            this.clear_erase.Text = "CE";
            this.clear_erase.UseVisualStyleBackColor = true;
            this.clear_erase.Click += new System.EventHandler(this.clear_erase_Click);
            // 
            // clear
            // 
            this.clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear.Location = new System.Drawing.Point(267, 224);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(50, 50);
            this.clear.TabIndex = 24;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 432);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.clear_erase);
            this.Controls.Add(this.trenutnaFunkcija);
            this.Controls.Add(this.rezultat);
            this.Controls.Add(this.b_enter);
            this.Controls.Add(this.b_log);
            this.Controls.Add(this.b_div);
            this.Controls.Add(this.b_mult);
            this.Controls.Add(this.b_minus);
            this.Controls.Add(this.b_plus);
            this.Controls.Add(this.b_sqrt);
            this.Controls.Add(this.b_pow);
            this.Controls.Add(this.b_cos);
            this.Controls.Add(this.b_sin);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Kalkulator";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button b_sin;
        private System.Windows.Forms.Button b_cos;
        private System.Windows.Forms.Button b_pow;
        private System.Windows.Forms.Button b_sqrt;
        private System.Windows.Forms.Button b_plus;
        private System.Windows.Forms.Button b_minus;
        private System.Windows.Forms.Button b_mult;
        private System.Windows.Forms.Button b_div;
        private System.Windows.Forms.Button b_log;
        private System.Windows.Forms.Button b_enter;
        private System.Windows.Forms.TextBox rezultat;
        private System.Windows.Forms.Label trenutnaFunkcija;
        private System.Windows.Forms.Button clear_erase;
        private System.Windows.Forms.Button clear;
    }
}

