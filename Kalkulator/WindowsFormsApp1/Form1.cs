﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Kalkulator : Form
    {
        Double rez = 0;
        String funkcija = "";
        bool izvrsenaFunkcija = false;

        public Kalkulator()
        {
            InitializeComponent();
        }

        private void button_click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0" || izvrsenaFunkcija)
                rezultat.Text = "";

            Button button = (Button)sender;
            izvrsenaFunkcija = false;
            if (button.Text == ".")
            {
                if(!rezultat.Text.Contains("."))
                    rezultat.Text = rezultat.Text + button.Text;
            }
            else
            rezultat.Text = rezultat.Text + button.Text;
            
        }

        private void klik_operator(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (rez != 0)
            {
                b_enter.PerformClick();
                funkcija = button.Text;
                trenutnaFunkcija.Text = rez + " " + funkcija;
                izvrsenaFunkcija = true;
            }
            else
            {
                funkcija = button.Text;
                rez = Double.Parse(rezultat.Text);
                trenutnaFunkcija.Text = rez + " " + funkcija;
                izvrsenaFunkcija = true;
            }
        }

        private void clear_erase_Click(object sender, EventArgs e)
        {
            rezultat.Text = "0";
            rez = 0;
            trenutnaFunkcija.Text = "";
        }

        private void clear_Click(object sender, EventArgs e)
        {
            rezultat.Text = "0";
        }

        private void b_enter_Click(object sender, EventArgs e)
        {
            switch (funkcija)
            {
                case "+":
                    rezultat.Text = (rez + Double.Parse(rezultat.Text)).ToString();
                    break;
                case "-":
                    rezultat.Text = (rez - Double.Parse(rezultat.Text)).ToString();
                    break;
                case "*":
                    rezultat.Text = (rez * Double.Parse(rezultat.Text)).ToString();
                    break;
                case "/":
                    rezultat.Text = (rez / Double.Parse(rezultat.Text)).ToString();
                    break;
                case "sin":
                    rezultat.Text = Math.Sin(Double.Parse(rezultat.Text)).ToString();
                    break;
                case "cos":
                    rezultat.Text = Math.Cos(Double.Parse(rezultat.Text)).ToString();
                    break;
                case "pow":
                    rezultat.Text = Math.Pow(rez, Double.Parse(rezultat.Text)).ToString();
                    break;
                case "sqrt":
                    rezultat.Text = Math.Sqrt(rez).ToString();
                    break;
                case "log":
                    rezultat.Text = Math.Log(rez).ToString();
                    break;
                default:
                    break;
            }
            rez = Double.Parse(rezultat.Text);
            trenutnaFunkcija.Text = "";
        }
    }
}
